package co.com.centaurosbank.centaurosbank.Controladores;

import co.com.centaurosbank.centaurosbank.Modelos.Clientes;
import co.com.centaurosbank.centaurosbank.Modelos.CreditoConsumo;
import co.com.centaurosbank.centaurosbank.Modelos.Solicitud;
import co.com.centaurosbank.centaurosbank.Servicios.ServicioCredito;
import co.com.centaurosbank.centaurosbank.Servicios.servicioSolicitud;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.*;

@RestController
@RequestMapping("/")
public class CreditoController {

    @Autowired
    private ServicioCredito servicioCredito;

    @GetMapping("/verCreditos")
    public ModelAndView verCreditos(Model model, @ModelAttribute("message") String message, HttpServletRequest request) {
        List<CreditoConsumo> creditos = servicioCredito.getAllCreditos();
        model.addAttribute("creditos", creditos);
        model.addAttribute("message", message);

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            Map<String, Object> jsonResponse = new HashMap<>();
            jsonResponse.put("creditos", creditos);
            return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
        } else {
            return new ModelAndView("VerCreditos");
        }
    }

    @GetMapping("/crearCredito")
    public ModelAndView crearCredito(Model model, @ModelAttribute("message") String message, HttpServletRequest request) {
        CreditoConsumo credito = new CreditoConsumo();
        model.addAttribute("credito", credito);
        model.addAttribute("message", message);

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            return new ModelAndView(new MappingJackson2JsonView(), Collections.emptyMap());
        } else {
            return new ModelAndView("CreditoConsumo");
        }
    }

    @PostMapping("/guardarCredito")
    public ModelAndView guardarCredito(CreditoConsumo creditoConsumo, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (servicioCredito.solicitarCredito(creditoConsumo)) {
            redirectAttributes.addFlashAttribute("message", "saveOK");

            String acceptHeader = request.getHeader("Accept");

            if (acceptHeader != null && acceptHeader.contains("application/json")) {
                Map<String, Object> jsonResponse = new HashMap<>();
                jsonResponse.put("message", "saveOK");
                return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
            } else {
                return new ModelAndView("redirect:/verCredito/"+creditoConsumo.getId());
            }
        }
        redirectAttributes.addFlashAttribute("message", "saveError");
        return new ModelAndView("redirect:/");
    }

    @GetMapping("/verCredito/{id}")
    public ModelAndView verCredito(@PathVariable Integer id, @ModelAttribute("message") String message, HttpServletRequest request) {
        Optional<CreditoConsumo> credito= servicioCredito.getCreditoById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("credito", credito.orElse(null));
        modelAndView.addObject("message", message);

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            Map<String, Object> jsonResponse = new HashMap<>();
            if (credito.isPresent()) {
                jsonResponse.put("solicitud", credito.get());
            } else {
                jsonResponse.put("error", "credito no encontrado");
            }
            return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
        } else {
            modelAndView.setViewName("VerCredito");
            return modelAndView;
        }
    }


    @DeleteMapping("/eliminarCredito/{id}")
    public ModelAndView eliminarCredito(@PathVariable Integer id, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (servicioCredito.deleteCreditoById(id)) {
            redirectAttributes.addFlashAttribute("message", "deleteOK");
        } else {
            redirectAttributes.addFlashAttribute("message", "deleteError");
        }

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            Map<String, Object> jsonResponse = new HashMap<>();
            jsonResponse.put("message", "deleteOK");
            return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
        } else {
            return new ModelAndView("redirect:/");
        }
    }

}
