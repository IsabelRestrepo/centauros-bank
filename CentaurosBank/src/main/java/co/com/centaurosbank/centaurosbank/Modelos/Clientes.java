package co.com.centaurosbank.centaurosbank.Modelos;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class Clientes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Integer NroDoc;
    private String TipoDoc;
    private String Nombres;
    private String Apellidos;
    private String Correo;
    private String Telefono;
    private LocalDate FechaNacimiento;
    private String Direccion;
    private double Ingresos;
    private String Ocupacion;
    private String EstadoCivil;
    private String Genero;
    private String Tipo ;

    public Clientes(int id, Integer nroDoc, String tipoDoc, String nombres, String apellidos, String correo, String telefono, String direccion, double ingresos, String ocupacion) {
        this.id = id;
        NroDoc = nroDoc;
        TipoDoc = tipoDoc;
        Nombres = nombres;
        Apellidos = apellidos;
        Correo = correo;
        Telefono = telefono;
        Direccion = direccion;
        Ingresos = ingresos;
        Ocupacion = ocupacion;
    }
}