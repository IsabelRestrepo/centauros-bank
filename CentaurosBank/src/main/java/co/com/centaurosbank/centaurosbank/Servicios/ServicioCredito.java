package co.com.centaurosbank.centaurosbank.Servicios;

import co.com.centaurosbank.centaurosbank.Modelos.CreditoConsumo;
import co.com.centaurosbank.centaurosbank.Repositorio.CreditoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ServicioCredito {
    @Autowired
    CreditoRepositorio creditoRepositorio;

    public boolean solicitarCredito(CreditoConsumo creditoConsumo) {
        CreditoConsumo creditoConsumo1 = creditoRepositorio.save(creditoConsumo);
        if (creditoRepositorio.findById(creditoConsumo1.getId()) != null) {
            return true;
        }
        return false;
    }

    public Optional<CreditoConsumo> getCreditoById(Integer id) {
        Optional<CreditoConsumo> creditoById = creditoRepositorio.findById(id);
        creditoById.orElseThrow(() -> new IllegalArgumentException("Not found"));
        return creditoById;
    }

    public List<CreditoConsumo> getAllCreditos() {
        List<CreditoConsumo> allSolicitudes = new ArrayList<>();
        creditoRepositorio.findAll().forEach(solicitud -> allSolicitudes.add(solicitud));
        return allSolicitudes;
    }


    public boolean deleteCreditoById(Integer id) {
        if (creditoRepositorio.existsById(id)) {
            creditoRepositorio.deleteById(id);
            return true;
        }
        return false;
    }

}
