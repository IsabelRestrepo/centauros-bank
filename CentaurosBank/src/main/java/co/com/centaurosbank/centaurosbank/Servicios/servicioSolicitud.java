package co.com.centaurosbank.centaurosbank.Servicios;

import co.com.centaurosbank.centaurosbank.Modelos.Clientes;
import co.com.centaurosbank.centaurosbank.Modelos.Solicitud;
import co.com.centaurosbank.centaurosbank.Repositorio.SolicitudRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class servicioSolicitud {
    @Autowired
    SolicitudRepositorio solicitudRepositorio;

    public boolean crearSolicitud(Solicitud solicitud) {
        solicitud.setFranquicia(solicitud.franquicia());
        solicitud.setCupo(solicitud.cupo());
        Solicitud solicitudes = solicitudRepositorio.save(solicitud);
        if (solicitudRepositorio.findById(solicitudes.getId()) != null) {
            return true;
        }
        return false;
    }

    public Optional<Solicitud> GetSolicitudById(Integer id) {
        Optional<Solicitud> solicitudById = solicitudRepositorio.findById(id);
        solicitudById.orElseThrow(() -> new IllegalArgumentException("Not found"));
        return solicitudById;
    }

    public List<Clientes> getAllSolicitudes() {
        List<Clientes> allSolicitudes = new ArrayList<>();
        solicitudRepositorio.findAll().forEach(solicitud -> allSolicitudes.add(solicitud));
        return allSolicitudes;
    }



    public boolean deleteSolicitudById(Integer id) {
        if (solicitudRepositorio.existsById(id)) {
            solicitudRepositorio.deleteById(id);
            return true;
        }
        return false;
    }

}
