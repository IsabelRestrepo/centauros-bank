package co.com.centaurosbank.centaurosbank.Modelos;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;


@Setter
@Getter
@Entity
@Table(name="creditos")
@NoArgsConstructor

public class CreditoConsumo extends Clientes {
    public CreditoConsumo(int id, Integer NroDoc, String TipoDoc, String Nombres, String Apellidos, String Correo, String Telefono,String Direccion, double Ingresos, String Ocupacion) {
        super(id, NroDoc, TipoDoc, Nombres, Apellidos, Correo, Telefono, Direccion, Ingresos, Ocupacion);
    }
    protected String Motivo;
    protected String TipoContrato;
    protected String MontoSolicitud;

}