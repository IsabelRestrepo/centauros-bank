package co.com.centaurosbank.centaurosbank.Controladores;

import co.com.centaurosbank.centaurosbank.Modelos.Clientes;
import co.com.centaurosbank.centaurosbank.Modelos.Solicitud;
import co.com.centaurosbank.centaurosbank.Servicios.servicioSolicitud;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@RestController
@RequestMapping("/")
public class SolicitudController {

    @Autowired
    private servicioSolicitud serviceSolicitud;

    @GetMapping("/verSolicitudes")
    public ModelAndView verSolicitudes(Model model, @ModelAttribute("message") String message, HttpServletRequest request) {
        List<Clientes> solicitudes = serviceSolicitud.getAllSolicitudes();
        model.addAttribute("solicitudes", solicitudes);
        model.addAttribute("message", message);

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            Map<String, Object> jsonResponse = new HashMap<>();
            jsonResponse.put("solicitudes", solicitudes);
            return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
        } else {
            return new ModelAndView("VerTodasSolicitudes");
        }
    }

    @GetMapping("/crearSolicitud")
    public ModelAndView crearSolicitud(Model model, @ModelAttribute("message") String message, HttpServletRequest request) {
        Solicitud solicitud = new Solicitud();
        model.addAttribute("solicitud", solicitud);
        model.addAttribute("message", message);

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            return new ModelAndView(new MappingJackson2JsonView(), Collections.emptyMap());
        } else {
            return new ModelAndView("agregarSolicitud");
        }
    }

    @PostMapping("guardarSolicitud")
    public ModelAndView guardarSolicitud(Solicitud solicitud, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (serviceSolicitud.crearSolicitud(solicitud)) {
            redirectAttributes.addFlashAttribute("message", "saveOK");

            String acceptHeader = request.getHeader("Accept");

            if (acceptHeader != null && acceptHeader.contains("application/json")) {
                Map<String, Object> jsonResponse = new HashMap<>();
                jsonResponse.put("message", "saveOK");
                return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
            } else {
                return new ModelAndView("redirect:/verSolicitud/" + solicitud.getId());
            }
        }
        redirectAttributes.addFlashAttribute("message", "saveError");
        return new ModelAndView("redirect:/");
    }

    @GetMapping("/verSolicitud/{id}")
    public ModelAndView verSolicitud(@PathVariable Integer id, @ModelAttribute("message") String message, HttpServletRequest request) {
        Optional<Solicitud> solicitud = serviceSolicitud.GetSolicitudById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("solicitud", solicitud.orElse(null));
        modelAndView.addObject("message", message);

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            Map<String, Object> jsonResponse = new HashMap<>();
            if (solicitud.isPresent()) {
                jsonResponse.put("solicitud", solicitud.get());
            } else {
                jsonResponse.put("error", "Solicitud no encontrada");
            }
            return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
        } else {
            modelAndView.setViewName("VerSolicitud");
            return modelAndView;
        }
    }
    @GetMapping("/editarSolicitud/{id}")
    public ModelAndView editarSolicitud(@PathVariable Integer id, Model model, @ModelAttribute("message") String message, HttpServletRequest request) {
        Optional<Solicitud> solicitud = serviceSolicitud.GetSolicitudById(id);
        ModelAndView modelAndView = new ModelAndView();

        String acceptHeader = request.getHeader("Accept");

        if (solicitud.isPresent()) {
            model.addAttribute("solicitud", solicitud.get());
            model.addAttribute("message", message);

            if (acceptHeader != null && acceptHeader.contains("application/json")) {
                Map<String, Object> jsonResponse = new HashMap<>();
                jsonResponse.put("solicitud", solicitud.get());
                return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
            } else {
                modelAndView.setViewName("EditarSolicitud");
                return modelAndView;
            }
        } else {
            if (acceptHeader != null && acceptHeader.contains("application/json")) {
                Map<String, Object> jsonResponse = new HashMap<>();
                jsonResponse.put("error", "Solicitud no encontrada");
                return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
            } else {
                return new ModelAndView("errorView");
            }
        }
    }

    @PatchMapping("/actualizarSolicitud")
    public ModelAndView updateEmpresa(@ModelAttribute("solicitud") Solicitud solicitud, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (serviceSolicitud.crearSolicitud(solicitud)){
            redirectAttributes.addFlashAttribute("mensaje", "updateOK");

            String acceptHeader = request.getHeader("Accept");

            if (acceptHeader != null && acceptHeader.contains("application/json")) {
                Map<String, Object> jsonResponse = new HashMap<>();
                jsonResponse.put("mensaje", "updateOK");
                return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
            } else {
                return new ModelAndView("redirect:/verSolicitud/" + solicitud.getId());
            }
        }
        redirectAttributes.addFlashAttribute("mensaje", "updateError");
        return new ModelAndView("redirect:/" + solicitud.getId());
    }

    @DeleteMapping("/eliminarSolicitud/{id}")
    public ModelAndView eliminarSolicitud(@PathVariable Integer id, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (serviceSolicitud.deleteSolicitudById(id)) {
            redirectAttributes.addFlashAttribute("message", "deleteOK");
        } else {
            redirectAttributes.addFlashAttribute("message", "deleteError");
        }

        String acceptHeader = request.getHeader("Accept");

        if (acceptHeader != null && acceptHeader.contains("application/json")) {
            Map<String, Object> jsonResponse = new HashMap<>();
            jsonResponse.put("message", "deleteOK");
            return new ModelAndView(new MappingJackson2JsonView(), jsonResponse);
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    @GetMapping("estado")
    public ModelAndView solicitudEnProceso() {
        return new ModelAndView("EnProceso");
    }

    @GetMapping("")
    public ModelAndView home() {
        return new ModelAndView("Home");
    }
}
