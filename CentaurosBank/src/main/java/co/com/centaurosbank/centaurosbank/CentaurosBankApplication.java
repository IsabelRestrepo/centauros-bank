package co.com.centaurosbank.centaurosbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CentaurosBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentaurosBankApplication.class, args);
    }

}
