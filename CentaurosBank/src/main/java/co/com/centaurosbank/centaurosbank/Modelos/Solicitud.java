package co.com.centaurosbank.centaurosbank.Modelos;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;


@Setter
@Getter
@Entity
@Table(name="solicitudes")
@NoArgsConstructor

public class Solicitud extends Clientes {
    public Solicitud(int id, Integer NroDoc, String TipoDoc, String Nombres, String Apellidos, String Correo, String Telefono, LocalDate FechaNacimiento, String Direccion, double Ingresos, String Ocupacion, String EstadoCivil, String Genero, String Tipo) {
        super(id, NroDoc, TipoDoc, Nombres, Apellidos, Correo, Telefono, FechaNacimiento, Direccion, Ingresos, Ocupacion, EstadoCivil, Genero, Tipo);
    }

    private double Cupo = cupo();
    private String Franquicia = franquicia();
    private LocalDate FechaCaducidad = LocalDate.now().plusYears(3).plusMonths(2);
    private int cvc = (int) (Math.random() * 900) + 100;



    public double cupo(){
        double total=(getIngresos()*0.60);
        return total;
    }


    public String franquicia(){
        if ("Black".equals(getTipo())){
            return "VISA";
        }
        else{
            return "MASTERCARD";
        }
    }

}