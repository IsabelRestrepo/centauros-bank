package co.com.centaurosbank.centaurosbank.Repositorio;

import co.com.centaurosbank.centaurosbank.Modelos.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SolicitudRepositorio extends JpaRepository<Solicitud,Integer> {
}
