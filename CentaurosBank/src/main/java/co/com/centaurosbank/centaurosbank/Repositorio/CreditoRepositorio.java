package co.com.centaurosbank.centaurosbank.Repositorio;

import co.com.centaurosbank.centaurosbank.Modelos.CreditoConsumo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditoRepositorio extends JpaRepository<CreditoConsumo,Integer> {
}
